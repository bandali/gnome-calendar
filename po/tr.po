# Turkish translation for gnome-calendar.
# Copyright (C) 2015-2023 gnome-calendar's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-calendar package.
#
# Taner Orak <tnrork@gmail.com>, 2015.
# Necdet Yücel <necdetyucel@gmail.com>, 2015.
# Furkan Ahmet Kara <furkanahmetkara.fk@gmail.com>, 2017.
# Muhammet Kara <muhammetk@gmail.com>, 2015, 2016, 2017.
# Çağrı Dolaz <dolazcagri@gmail.com>, 2017.
# Sabri Ünal <libreajans@gmail.com>, 2019, 2020, 2022, 2023.
# Emin Tufan Çetin <etcetin@gmail.com>, 2017, 2018, 2020, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-calendar master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-calendar/issues\n"
"POT-Creation-Date: 2023-03-03 22:56+0000\n"
"PO-Revision-Date: 2023-03-04 21:41+0300\n"
"Last-Translator: Sabri Ünal <libreajans@gmail.com>\n"
"Language-Team: Türkçe <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.1.1\n"
"X-POOTLE-MTIME: 1433367259.000000\n"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:7
#: data/org.gnome.Calendar.desktop.in.in:3 src/main.c:35
#: src/gui/gcal-application.c:178 src/gui/gcal-quick-add-popover.ui:135
#: src/gui/gcal-window.ui:4
msgid "Calendar"
msgstr "Takvim"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:8
msgid "Calendar for GNOME"
msgstr "GNOME için Takvim"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:11
msgid ""
"GNOME Calendar is a simple and beautiful calendar application designed to "
"perfectly fit the GNOME desktop. By reusing the components which the GNOME "
"desktop is built on, Calendar nicely integrates with the GNOME ecosystem."
msgstr ""
"GNOME Takvim, GNOME masaüstüne kusursuzca uymak üzere tasarlanmış basit ve "
"güzel takvim uygulamasıdır. GNOME masaüstünün kurulu olduğu bileşenleri "
"yeniden kullanan Takvim, GNOME ekosistemi ile uyum içindedir."

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:16
msgid ""
"We aim to find the perfect balance between nicely crafted features and user-"
"centred usability. No excess, nothing missing. You’ll feel comfortable using "
"Calendar, like you’ve been using it for ages!"
msgstr ""
"Hoş özellikler ve kullanıcı odaklı kullanılabilirlik arasında kusursuz "
"dengeyi sağlamayı amaçlıyoruz. Ne eksik, ne çok. Takvim’i kullanırken güya "
"onu yıllardır kullanıyormuş gibi rahat hissedeceksiniz!"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:24
msgid "Week view"
msgstr "Hafta görünümü"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:28
msgid "Month view"
msgstr "Ay görünümü"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:32
msgid "Event editor"
msgstr "Olay düzenleyici"

#: data/org.gnome.Calendar.desktop.in.in:4
msgid "Access and manage your calendars"
msgstr "Takvimlerinize erişin ve yönetin"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calendar.desktop.in.in:13
msgid "Calendar;Event;Reminder;"
msgstr "Takvim;Olay;Etkinlik;Anımsatıcı;Hatırlatıcı;"

#: data/org.gnome.calendar.gschema.xml.in:6
msgid "Window maximized"
msgstr "Pencere ekranı kapladı"

#: data/org.gnome.calendar.gschema.xml.in:7
msgid "Window maximized state"
msgstr "Pencere ekranı kaplamış durumda"

#: data/org.gnome.calendar.gschema.xml.in:11
msgid "Window size"
msgstr "Pencere boyutu"

#: data/org.gnome.calendar.gschema.xml.in:12
msgid "Window size (width and height)."
msgstr "Pencere boyutu (genişlik ve yükseklik)."

#: data/org.gnome.calendar.gschema.xml.in:16
msgid "Window position"
msgstr "Pencere konumu"

#: data/org.gnome.calendar.gschema.xml.in:17
msgid "Window position (x and y)."
msgstr "Pencere konumu (x ve y)."

#: data/org.gnome.calendar.gschema.xml.in:21
msgid "Type of the active view"
msgstr "Aktif görünümün türü"

#: data/org.gnome.calendar.gschema.xml.in:22
msgid "Type of the active window view, default value is: monthly view"
msgstr "Aktif pencere görünümü türü, öntanımlı değer: aylık görünüm"

#: data/org.gnome.calendar.gschema.xml.in:26
msgid "Weather Service Configuration"
msgstr "Hava Durumu Hizmeti Yapılandırma"

#: data/org.gnome.calendar.gschema.xml.in:27
msgid ""
"Whether weather reports are shown, automatic locations are used and a "
"location-name"
msgstr ""
"Hava durumu bildirilerinin gösterilmesi, kendiliğinden saptanmış konumlar "
"kullanılır ve bir konum adı"

#. Translators: %1$s is the start date and %2$s is the end date.
#. Translators: %1$s is the start date, and %2$s. For example: June 21 - November 29, 2022
#: src/core/gcal-event.c:1906 src/gui/gcal-event-popover.c:395
#, c-format
msgid "%1$s — %2$s"
msgstr "%1$s — %2$s"

#.
#. * Translators: %1$s is the start date, %2$s is the start time,
#. * %3$s is the end date, and %4$s is the end time.
#.
#: src/core/gcal-event.c:1914
#, c-format
msgid "%1$s %2$s — %3$s %4$s"
msgstr "%1$s %2$s — %3$s %4$s"

#. Translators: %1$s is a date, %2$s is the start hour, and %3$s is the end hour
#. Translators: %1$s is the event name, %2$s is the start hour, and %3$s is the end hour
#: src/core/gcal-event.c:1930 src/gui/gcal-quick-add-popover.c:461
#, c-format
msgid "%1$s, %2$s – %3$s"
msgstr "%1$s, %2$s – %3$s"

#: src/gui/calendar-management/gcal-calendar-management-dialog.ui:4
msgid "Calendar Settings"
msgstr "Takvim Ayarları"

#: src/gui/calendar-management/gcal-calendars-page.c:354
msgid "Manage Calendars"
msgstr "Takvimleri Yönet"

#. Update notification label
#: src/gui/calendar-management/gcal-calendars-page.c:384
#, c-format
msgid "Calendar <b>%s</b> removed"
msgstr "<b>%s</b> takvimi silindi"

#: src/gui/calendar-management/gcal-calendars-page.ui:24
msgid "Undo"
msgstr "Geri Al"

#: src/gui/calendar-management/gcal-calendars-page.ui:78
msgid "Add Calendar…"
msgstr "Takvim Ekle…"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:19
msgid "Account"
msgstr "Hesap"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:46
msgid "Settings"
msgstr "Ayarlar"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:57
#: src/gui/event-editor/gcal-summary-section.ui:23
#: src/gui/gcal-event-popover.ui:111
#: src/gui/importer/gcal-import-file-row.c:151
msgid "Location"
msgstr "Konum"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:84
msgid "Calendar name"
msgstr "Takvim adı"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:109
#: src/gui/calendar-management/gcal-new-calendar-page.ui:33
msgid "Color"
msgstr "Renk"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:134
msgid "Display calendar"
msgstr "Takvimi görüntüle"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:146
msgid "Add new events to this calendar by default"
msgstr "Yeni olayları öntanımlı olarak bu takvime ekle"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:158
msgid "Remove Calendar"
msgstr "Takvimi Kaldır"

#: src/gui/calendar-management/gcal-file-chooser-button.c:49
msgid "Select a file"
msgstr "Dosya seç"

#: src/gui/calendar-management/gcal-file-chooser-button.c:94
#: src/gui/calendar-management/gcal-new-calendar-page.ui:188
#: src/gui/calendar-management/gcal-new-calendar-page.ui:290
#: src/gui/event-editor/gcal-event-editor-dialog.ui:15
msgid "Cancel"
msgstr "Vazgeç"

#: src/gui/calendar-management/gcal-file-chooser-button.c:96
msgid "Open"
msgstr "Aç"

#: src/gui/calendar-management/gcal-new-calendar-page.c:517
msgid "New Calendar"
msgstr "Yeni Takvim"

#: src/gui/calendar-management/gcal-new-calendar-page.c:695
msgid "Calendar files"
msgstr "Takvim dosyaları"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:16
#: src/gui/calendar-management/gcal-new-calendar-page.ui:22
msgid "Calendar Name"
msgstr "Takvim Adı"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:55
msgid "Import a Calendar"
msgstr "Takvimi İçe Aktar"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:72
msgid ""
"Alternatively, enter the web address of an online calendar you want to "
"import, or open a supported calendar file."
msgstr ""
"Diğer seçenek olarak, içe aktarmak istediğiniz çevrim içi takvimin internet "
"adresini girin veya desteklenen takvim dosyasını açın."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:100
msgid "Open a File"
msgstr "Dosya Aç"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:124
msgid "Calendars"
msgstr "Takvimler"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:148
msgid ""
"If the calendar belongs to one of your online accounts, you can add it "
"through the <a href=\"GOA\">online account settings</a>."
msgstr ""
"Eğer takvim çevrim içi hesaplarınızdan birininse, <a href=\"GOA\">çevrim içi "
"hesap ayarları</a>ndan ekleyebilirsiniz."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:175
msgid "Credentials"
msgstr "Kimlik Bilgileri"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:196
msgid "Connect"
msgstr "Bağlan"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:225
msgid "User"
msgstr "Kullanıcı"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:238
msgid "Password"
msgstr "Parola"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:294
#: src/gui/gcal-quick-add-popover.ui:98
msgid "Add"
msgstr "Ekle"

#: src/gui/event-editor/gcal-alarm-row.c:84
#, c-format
msgid "%1$u day, %2$u hour, and %3$u minute before"
msgid_plural "%1$u day, %2$u hour, and %3$u minutes before"
msgstr[0] "%1$u gün, %2$u saat ve %3$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:88
#, c-format
msgid "%1$u day, %2$u hours, and %3$u minute before"
msgid_plural "%1$u day, %2$u hours, and %3$u minutes before"
msgstr[0] "%1$u gün, %2$u saat ve %3$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:94
#, c-format
msgid "%1$u days, %2$u hour, and %3$u minute before"
msgid_plural "%1$u days, %2$u hour, and %3$u minutes before"
msgstr[0] "%1$u gün, %2$u saat ve %3$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:98
#, c-format
msgid "%1$u days, %2$u hours, and %3$u minute before"
msgid_plural "%1$u days, %2$u hours, and %3$u minutes before"
msgstr[0] "%1$u gün, %2$u saat ve %3$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:113
#, c-format
msgid "%1$u day and %2$u hour before"
msgid_plural "%1$u day and %2$u hours before"
msgstr[0] "%1$u gün ve %2$u saat önce"

#: src/gui/event-editor/gcal-alarm-row.c:117
#, c-format
msgid "%1$u days and %2$u hour before"
msgid_plural "%1$u days and %2$u hours before"
msgstr[0] "%1$u gün ve %2$u saat önce"

#: src/gui/event-editor/gcal-alarm-row.c:133
#, c-format
msgid "%1$u day and %2$u minute before"
msgid_plural "%1$u day and %2$u minutes before"
msgstr[0] "%1$u gün ve %2$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:137
#, c-format
msgid "%1$u days and %2$u minute before"
msgid_plural "%1$u days and %2$u minutes before"
msgstr[0] "%1$u gün ve %2$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:147
#, c-format
msgid "%1$u day before"
msgid_plural "%1$u days before"
msgstr[0] "%1$u gün önce"

#: src/gui/event-editor/gcal-alarm-row.c:165
#, c-format
msgid "%1$u hour and %2$u minute before"
msgid_plural "%1$u hour and %2$u minutes before"
msgstr[0] "%1$u saat ve %2$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:169
#, c-format
msgid "%1$u hours and %2$u minute before"
msgid_plural "%1$u hours and %2$u minutes before"
msgstr[0] "%1$u saat ve %2$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:179
#, c-format
msgid "%1$u hour before"
msgid_plural "%1$u hours before"
msgstr[0] "%1$u saat önce"

#: src/gui/event-editor/gcal-alarm-row.c:191
#, c-format
msgid "%1$u minute before"
msgid_plural "%1$u minutes before"
msgstr[0] "%1$u dakika önce"

#: src/gui/event-editor/gcal-alarm-row.c:198
msgid "Event start time"
msgstr "Olay başlangıç zamanı"

#: src/gui/event-editor/gcal-alarm-row.ui:11
msgid "Toggles the sound of the alarm"
msgstr "Alarmın sesini açar/kapar"

#: src/gui/event-editor/gcal-alarm-row.ui:21
msgid "Remove the alarm"
msgstr "Alarmı kaldır"

#: src/gui/event-editor/gcal-event-editor-dialog.c:196
msgid "Save"
msgstr "Kaydet"

#: src/gui/event-editor/gcal-event-editor-dialog.c:196
#: src/gui/event-editor/gcal-event-editor-dialog.ui:86
msgid "Done"
msgstr "Tamam"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:29
msgid "Click to select the calendar"
msgstr "Takvim seçmek için tıkla"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:115
msgid "Schedule"
msgstr "Zamanlama"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:127
msgid "Reminders"
msgstr "Anımsatıcılar"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:139
msgid "Notes"
msgstr "Notlar"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:159
msgid "Delete Event"
msgstr "Olayı Sil"

#: src/gui/event-editor/gcal-reminders-section.ui:26
msgid "Add a Reminder…"
msgstr "Anımsatıcı Ekle…"

#: src/gui/event-editor/gcal-reminders-section.ui:52
msgid "5 minutes"
msgstr "5 dakika"

#: src/gui/event-editor/gcal-reminders-section.ui:58
msgid "10 minutes"
msgstr "10 dakika"

#: src/gui/event-editor/gcal-reminders-section.ui:64
msgid "15 minutes"
msgstr "15 dakika"

#: src/gui/event-editor/gcal-reminders-section.ui:70
msgid "30 minutes"
msgstr "30 dakika"

#: src/gui/event-editor/gcal-reminders-section.ui:76
msgid "1 hour"
msgstr "1 saat"

#: src/gui/event-editor/gcal-reminders-section.ui:82
msgid "1 day"
msgstr "1 gün"

#: src/gui/event-editor/gcal-reminders-section.ui:88
msgid "2 days"
msgstr "2 gün"

#: src/gui/event-editor/gcal-reminders-section.ui:94
msgid "3 days"
msgstr "3 gün"

#: src/gui/event-editor/gcal-reminders-section.ui:100
msgid "1 week"
msgstr "1 hafta"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:238
#, c-format
msgid "Last %A"
msgstr "Geçen %A"

#: src/gui/event-editor/gcal-schedule-section.c:242
#: src/gui/gcal-event-popover.c:207 src/gui/gcal-event-popover.c:321
#: src/gui/views/gcal-agenda-view.c:186
msgid "Yesterday"
msgstr "Dün"

#: src/gui/event-editor/gcal-schedule-section.c:246
#: src/gui/gcal-event-popover.c:199 src/gui/gcal-event-popover.c:313
#: src/gui/views/gcal-agenda-view.c:182
msgid "Today"
msgstr "Bugün"

#: src/gui/event-editor/gcal-schedule-section.c:250
#: src/gui/gcal-event-popover.c:203 src/gui/gcal-event-popover.c:317
#: src/gui/views/gcal-agenda-view.c:184
msgid "Tomorrow"
msgstr "Yarın"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:255
#, c-format
msgid "This %A"
msgstr "Bu %A"

#.
#. * Translators: %1$s is the formatted date (e.g. Today, Sunday, or even 2019-10-11) and %2$s is the
#. * formatted time (e.g. 03:14 PM, or 21:29)
#.
#: src/gui/event-editor/gcal-schedule-section.c:285
#, c-format
msgid "%1$s, %2$s"
msgstr "%1$s, %2$s"

#: src/gui/event-editor/gcal-schedule-section.ui:17
msgid "All Day"
msgstr "Tüm Gün"

#: src/gui/event-editor/gcal-schedule-section.ui:34
#: src/gui/importer/gcal-import-file-row.c:152
msgid "Starts"
msgstr "Başlama"

#: src/gui/event-editor/gcal-schedule-section.ui:77
#: src/gui/importer/gcal-import-file-row.c:153
msgid "Ends"
msgstr "Bitiş"

#: src/gui/event-editor/gcal-schedule-section.ui:120
msgid "Repeat"
msgstr "Yinele"

#: src/gui/event-editor/gcal-schedule-section.ui:128
msgid "No Repeat"
msgstr "Yineleme"

#: src/gui/event-editor/gcal-schedule-section.ui:129
msgid "Daily"
msgstr "Günlük"

#: src/gui/event-editor/gcal-schedule-section.ui:130
msgid "Monday – Friday"
msgstr "Pazartesi – Cuma"

#: src/gui/event-editor/gcal-schedule-section.ui:131
msgid "Weekly"
msgstr "Haftalık"

#: src/gui/event-editor/gcal-schedule-section.ui:132
msgid "Monthly"
msgstr "Aylık"

#: src/gui/event-editor/gcal-schedule-section.ui:133
msgid "Yearly"
msgstr "Yıllık"

#: src/gui/event-editor/gcal-schedule-section.ui:146
msgid "End Repeat"
msgstr "Yineleme Bitişi"

#: src/gui/event-editor/gcal-schedule-section.ui:154
msgid "Forever"
msgstr "Sonsuza Dek"

#: src/gui/event-editor/gcal-schedule-section.ui:155
msgid "No. of occurrences"
msgstr "Oluş sayısı"

#: src/gui/event-editor/gcal-schedule-section.ui:156
msgid "Until Date"
msgstr "Belirli Tarihe Dek"

#: src/gui/event-editor/gcal-schedule-section.ui:169
msgid "Number of Occurrences"
msgstr "Oluş Sayısı"

#: src/gui/event-editor/gcal-schedule-section.ui:188
msgid "End Repeat Date"
msgstr "Yineleme Bitiş Tarihi"

#: src/gui/event-editor/gcal-summary-section.c:78
#: src/gui/gcal-quick-add-popover.c:676
msgid "Unnamed event"
msgstr "Adsız olay"

#: src/gui/event-editor/gcal-summary-section.ui:16
#: src/gui/importer/gcal-import-file-row.c:150
msgid "Title"
msgstr "Başlık"

#: src/gui/event-editor/gcal-time-selector.ui:22
msgid ":"
msgstr ":"

#: src/gui/event-editor/gcal-time-selector.ui:46
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "AM"
msgstr "ÖÖ"

#: src/gui/event-editor/gcal-time-selector.ui:47
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "PM"
msgstr "ÖS"

#: src/gui/gcal-application.c:58
msgid "Quit GNOME Calendar"
msgstr "GNOME Takvimʼden Çık"

#: src/gui/gcal-application.c:63
msgid "Display version number"
msgstr "Sürüm numarasını göster"

#: src/gui/gcal-application.c:68
msgid "Enable debug messages"
msgstr "Hata ayıklama iletilerini etkinleştir"

#: src/gui/gcal-application.c:73
msgid "Open calendar on the passed date"
msgstr "Takvimi geçmiş tarihte aç"

#: src/gui/gcal-application.c:78
msgid "Open calendar showing the passed event"
msgstr "Takvimi, geçmiş olayı göstererek aç"

#: src/gui/gcal-application.c:144
#, c-format
msgid "Copyright © 2012–%d The Calendar authors"
msgstr "Telif Hakkı © 2012–%d Takvim geliştiricileri"

#: src/gui/gcal-application.c:180
msgid "The GNOME Project"
msgstr "GNOME Projesi"

#: src/gui/gcal-application.c:189
msgid "translator-credits"
msgstr ""
"Emin Tufan Çetin <etcetin@gmail.com>\n"
"Muhammet Kara <muhammetk@gmail.com>"

#: src/gui/gcal-application.c:194 src/gui/gcal-window.ui:283
msgid "Weather"
msgstr "Hava Durumu"

#: src/gui/gcal-calendar-button.ui:6
msgid "Manage your calendars"
msgstr "Takvimlerinizi yönetin"

#: src/gui/gcal-calendar-button.ui:23
msgid "_Calendars"
msgstr "_Takvimler"

#: src/gui/gcal-calendar-button.ui:42
msgctxt "tooltip"
msgid "Synchronizing remote calendars…"
msgstr "Uzak takvimler eşzamanlanıyor…"

#: src/gui/gcal-calendar-button.ui:72
msgid "_Synchronize Calendars"
msgstr "Takvimleri _Eşzamanla"

#: src/gui/gcal-calendar-button.ui:76
msgid "Manage Calendars…"
msgstr "Takvimleri Yönet…"

#: src/gui/gcal-event-popover.c:122 src/gui/gcal-quick-add-popover.c:255
msgid "January"
msgstr "Ocak"

#: src/gui/gcal-event-popover.c:123 src/gui/gcal-quick-add-popover.c:256
msgid "February"
msgstr "Şubat"

#: src/gui/gcal-event-popover.c:124 src/gui/gcal-quick-add-popover.c:257
msgid "March"
msgstr "Mart"

#: src/gui/gcal-event-popover.c:125 src/gui/gcal-quick-add-popover.c:258
msgid "April"
msgstr "Nisan"

#: src/gui/gcal-event-popover.c:126 src/gui/gcal-quick-add-popover.c:259
msgid "May"
msgstr "Mayıs"

#: src/gui/gcal-event-popover.c:127 src/gui/gcal-quick-add-popover.c:260
msgid "June"
msgstr "Haziran"

#: src/gui/gcal-event-popover.c:128 src/gui/gcal-quick-add-popover.c:261
msgid "July"
msgstr "Temmuz"

#: src/gui/gcal-event-popover.c:129 src/gui/gcal-quick-add-popover.c:262
msgid "August"
msgstr "Ağustos"

#: src/gui/gcal-event-popover.c:130 src/gui/gcal-quick-add-popover.c:263
msgid "September"
msgstr "Eylül"

#: src/gui/gcal-event-popover.c:131 src/gui/gcal-quick-add-popover.c:264
msgid "October"
msgstr "Ekim"

#: src/gui/gcal-event-popover.c:132 src/gui/gcal-quick-add-popover.c:265
msgid "November"
msgstr "Kasım"

#: src/gui/gcal-event-popover.c:133 src/gui/gcal-quick-add-popover.c:266
msgid "December"
msgstr "Aralık"

#: src/gui/gcal-event-popover.c:158
#, c-format
msgid "Today %s"
msgstr "Bugün %s"

#: src/gui/gcal-event-popover.c:162
#, c-format
msgid "Tomorrow %s"
msgstr "Yarın %s"

#: src/gui/gcal-event-popover.c:166
#, c-format
msgid "Yesterday %s"
msgstr "Dün %s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$ is the hour. This format string results in dates
#. * like "November 21, 22:00".
#.
#: src/gui/gcal-event-popover.c:175
#, c-format
msgid "%1$s %2$d, %3$s"
msgstr "%2$d %1$s, %3$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, and %4$s is the hour. This format string
#. * results in dates like "November 21, 2020, 22:00".
#.
#: src/gui/gcal-event-popover.c:187
#, c-format
msgid "%1$s %2$d, %3$d, %4$s"
msgstr "%2$d %1$s, %3$d, %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), and %2$d is
#. * the day of month. This format string results in dates like
#. * "November 21".
#.
#: src/gui/gcal-event-popover.c:216 src/gui/gcal-event-popover.c:330
#, c-format
msgid "%1$s %2$d"
msgstr "%2$d %1$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$d is the year. This format string results in dates
#. * like "November 21, 2020".
#.
#: src/gui/gcal-event-popover.c:227 src/gui/gcal-event-popover.c:341
#, c-format
msgid "%1$s %2$d, %3$d"
msgstr "%2$d %1$s, %3$d"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Today, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:261
#, c-format
msgid "Today, %1$s — %2$s"
msgstr "Bugün, %1$s — %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:269
#, c-format
msgid "Tomorrow, %1$s – %2$s"
msgstr "Yarın, %1$s – %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:277
#, c-format
msgid "Yesterday, %1$s – %2$s"
msgstr "Dün, %1$s – %2$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$s is the start hour, and %4$s is the end hour. This
#. * format string results in dates like "November 21, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:286
#, c-format
msgid "%1$s %2$d, %3$s – %4$s"
msgstr "%2$d %1$s, %3$s – %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, %4$s is the start hour, and %5$s is the
#. * end hour. This format string results in dates like:
#. *
#. * "November 21, 2021, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:301
#, c-format
msgid "%1$s %2$d, %3$d, %4$s – %5$s"
msgstr "%2$d %1$s, %3$d, %4$s – %5$s"

#: src/gui/gcal-event-popover.ui:71
msgid "No event information"
msgstr "Olay bilgisi yok"

#: src/gui/gcal-event-popover.ui:169
msgid "Edit…"
msgstr "Düzenle…"

#. Translators: %s is the location of the event (e.g. "Downtown, 3rd Avenue")
#: src/gui/gcal-event-widget.c:351
#, c-format
msgid "At %s"
msgstr "%sʼde"

#: src/gui/gcal-meeting-row.c:65
msgid "Google Meet"
msgstr "Google Meet"

#: src/gui/gcal-meeting-row.c:66
msgid "Jitsi"
msgstr "Jitsi"

#: src/gui/gcal-meeting-row.c:67
msgid "Whereby"
msgstr "Whereby"

#: src/gui/gcal-meeting-row.c:68
msgid "Zoom"
msgstr "Zoom"

#: src/gui/gcal-meeting-row.c:69
msgid "Microsoft Teams"
msgstr "Microsoft Teams"

#: src/gui/gcal-meeting-row.c:79
msgid "Unknown Service"
msgstr "Bilinmeyen Servis"

#. Translators: "Join" as in "Join meeting"
#: src/gui/gcal-meeting-row.ui:12
msgid "Join"
msgstr "Katıl"

#: src/gui/gcal-quick-add-popover.c:118
#, c-format
msgid "%s (this calendar is read-only)"
msgstr "%s (bu takvim salt-okunur)"

#: src/gui/gcal-quick-add-popover.c:233
msgid "from next Monday"
msgstr "gelecek pazartesinden başlayarak"

#: src/gui/gcal-quick-add-popover.c:234
msgid "from next Tuesday"
msgstr "gelecek salıdan başlayarak"

#: src/gui/gcal-quick-add-popover.c:235
msgid "from next Wednesday"
msgstr "gelecek çarşambadan başlayarak"

#: src/gui/gcal-quick-add-popover.c:236
msgid "from next Thursday"
msgstr "gelecek perşembeden başlayarak"

#: src/gui/gcal-quick-add-popover.c:237
msgid "from next Friday"
msgstr "gelecek cumadan başlayarak"

#: src/gui/gcal-quick-add-popover.c:238
msgid "from next Saturday"
msgstr "gelecek cumartesiden başlayarak"

#: src/gui/gcal-quick-add-popover.c:239
msgid "from next Sunday"
msgstr "gelecek pazardan başlayarak"

#: src/gui/gcal-quick-add-popover.c:244
msgid "to next Monday"
msgstr "gelecek pazartesiye"

#: src/gui/gcal-quick-add-popover.c:245
msgid "to next Tuesday"
msgstr "gelecek salıya"

#: src/gui/gcal-quick-add-popover.c:246
msgid "to next Wednesday"
msgstr "gelecek çarşambaya"

#: src/gui/gcal-quick-add-popover.c:247
msgid "to next Thursday"
msgstr "gelecek perşembeye"

#: src/gui/gcal-quick-add-popover.c:248
msgid "to next Friday"
msgstr "gelecek cumaya"

#: src/gui/gcal-quick-add-popover.c:249
msgid "to next Saturday"
msgstr "gelecek cumartesiye"

#: src/gui/gcal-quick-add-popover.c:250
msgid "to next Sunday"
msgstr "gelecek pazara"

#: src/gui/gcal-quick-add-popover.c:275
#, c-format
msgid "from Today"
msgstr "bugünden başlayarak"

#: src/gui/gcal-quick-add-popover.c:279
#, c-format
msgid "from Tomorrow"
msgstr "yarından başlayarak"

#: src/gui/gcal-quick-add-popover.c:283
#, c-format
msgid "from Yesterday"
msgstr "dünden başlayarak"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:301
#, c-format
msgid "from %1$s %2$s"
msgstr "%1$s %2$sden başlayarak"

#: src/gui/gcal-quick-add-popover.c:312
#, c-format
msgid "to Today"
msgstr "bugüne"

#: src/gui/gcal-quick-add-popover.c:316
#, c-format
msgid "to Tomorrow"
msgstr "yarına"

#: src/gui/gcal-quick-add-popover.c:320
#, c-format
msgid "to Yesterday"
msgstr "düne"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:338
#, c-format
msgid "to %1$s %2$s"
msgstr "%1$s %2$se"

#. Translators: %1$s is the start date (e.g. "from Today") and %2$s is the end date (e.g. "to Tomorrow")
#: src/gui/gcal-quick-add-popover.c:345
#, c-format
msgid "New Event %1$s %2$s"
msgstr "%1$s %2$s’de Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:362
#, c-format
msgid "New Event Today"
msgstr "Bugün Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:366
#, c-format
msgid "New Event Tomorrow"
msgstr "Yarın Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:370
#, c-format
msgid "New Event Yesterday"
msgstr "Dün Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:376
msgid "New Event next Monday"
msgstr "Gelecek Pazartesi Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:377
msgid "New Event next Tuesday"
msgstr "Gelecek Salı Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:378
msgid "New Event next Wednesday"
msgstr "Gelecek Çarşamba Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:379
msgid "New Event next Thursday"
msgstr "Gelecek Perşembe Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:380
msgid "New Event next Friday"
msgstr "Gelecek Cuma Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:381
msgid "New Event next Saturday"
msgstr "Gelecek Cumartesi Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:382
msgid "New Event next Sunday"
msgstr "Gelecek Pazar Yeni Olay"

#. Translators: %d is the numeric day of month
#: src/gui/gcal-quick-add-popover.c:394
#, c-format
msgid "New Event on January %d"
msgstr "%d Ocak’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:395
#, c-format
msgid "New Event on February %d"
msgstr "%d Şubat’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:396
#, c-format
msgid "New Event on March %d"
msgstr "%d Mart’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:397
#, c-format
msgid "New Event on April %d"
msgstr "%d Nisan’da Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:398
#, c-format
msgid "New Event on May %d"
msgstr "%d Mayıs’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:399
#, c-format
msgid "New Event on June %d"
msgstr "%d Haziran’da Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:400
#, c-format
msgid "New Event on July %d"
msgstr "%d Temmuz’da Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:401
#, c-format
msgid "New Event on August %d"
msgstr "%d Ağustos’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:402
#, c-format
msgid "New Event on September %d"
msgstr "%d Eylül’de Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:403
#, c-format
msgid "New Event on October %d"
msgstr "%d Ekim’de Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:404
#, c-format
msgid "New Event on November %d"
msgstr "%d Kasım’da Yeni Olay"

#: src/gui/gcal-quick-add-popover.c:405
#, c-format
msgid "New Event on December %d"
msgstr "%d Aralık’ta Yeni Olay"

#: src/gui/gcal-quick-add-popover.ui:87
msgid "Edit Details…"
msgstr "Ayrıntıları Düzenle…"

#: src/gui/gcal-toolbar-end.ui:9
msgctxt "tooltip"
msgid "Search for events"
msgstr "Olay ara"

#: src/gui/gcal-toolbar-end.ui:21
msgctxt "tooltip"
msgid "Add a new event"
msgstr "Yeni olay ekle"

#: src/gui/gcal-weather-settings.ui:17
msgid "Show Weather"
msgstr "Hava Durumunu Göster"

#: src/gui/gcal-weather-settings.ui:34
msgid "Automatic Location"
msgstr "Konumu Kendiliğinden Sapta"

#: src/gui/gcal-window.c:713
msgid "Another event deleted"
msgstr "Başka bir olay silindi"

#: src/gui/gcal-window.c:713
msgid "Event deleted"
msgstr "Olay silindi"

#: src/gui/gcal-window.c:715
msgid "_Undo"
msgstr "_Geri Al"

#: src/gui/gcal-window.ui:117
msgid "Main Menu"
msgstr "Ana Menü"

#: src/gui/gcal-window.ui:156 src/gui/gcal-window.ui:189
msgid "_Today"
msgstr "_Bugün"

#: src/gui/gcal-window.ui:223
msgid "_Week"
msgstr "_Hafta"

#: src/gui/gcal-window.ui:238
msgid "_Month"
msgstr "_Ay"

#: src/gui/gcal-window.ui:279
msgid "_Online Accounts…"
msgstr "Çevrim İçi _Hesaplar…"

#: src/gui/gcal-window.ui:291
msgid "_Keyboard Shortcuts"
msgstr "_Klavye Kısayolları"

#: src/gui/gcal-window.ui:295
msgid "_About Calendar"
msgstr "Takvim _Hakkında"

#: src/gui/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Genel"

#: src/gui/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "New event"
msgstr "Yeni olay"

#: src/gui/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Close window"
msgstr "Pencereyi kapat"

#: src/gui/gtk/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Search"
msgstr "Ara"

#: src/gui/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Show help"
msgstr "Yardım göster"

#: src/gui/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "Kısayollar"

#: src/gui/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Gezinti"

#: src/gui/gtk/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Go back"
msgstr "Geri git"

#: src/gui/gtk/help-overlay.ui:55
msgctxt "shortcut window"
msgid "Go forward"
msgstr "İleri git"

#: src/gui/gtk/help-overlay.ui:61
msgctxt "shortcut window"
msgid "Show today"
msgstr "Bugünü göster"

#: src/gui/gtk/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Next view"
msgstr "Sonraki görünüm"

#: src/gui/gtk/help-overlay.ui:73
msgctxt "shortcut window"
msgid "Previous view"
msgstr "Önceki görünüm"

#: src/gui/gtk/help-overlay.ui:81
msgctxt "shortcut window"
msgid "View"
msgstr "Görünüm"

#: src/gui/gtk/help-overlay.ui:84
msgctxt "shortcut window"
msgid "Week view"
msgstr "Hafta görünümü"

#: src/gui/gtk/help-overlay.ui:90
msgctxt "shortcut window"
msgid "Month view"
msgstr "Ay görünümü"

#: src/gui/importer/gcal-import-dialog.c:396
#, c-format
msgid "Import %d event"
msgid_plural "Import %d events"
msgstr[0] "%d olayı içe aktar"

#: src/gui/importer/gcal-import-dialog.ui:4
msgid "Import Files…"
msgstr "Dosyaları İçe Aktar…"

#: src/gui/importer/gcal-import-dialog.ui:20 src/utils/gcal-utils.c:1320
msgid "_Cancel"
msgstr "İ_ptal Et"

#: src/gui/importer/gcal-import-dialog.ui:29
msgid "_Import"
msgstr "İ_çe Aktar"

#: src/gui/importer/gcal-import-dialog.ui:50
msgid "C_alendar"
msgstr "T_akvim"

#: src/gui/importer/gcal-importer.c:33
msgid "No error"
msgstr "Hata yok"

#: src/gui/importer/gcal-importer.c:36
msgid "Bad argument to function"
msgstr "İşleve hatalı argüman"

#: src/gui/importer/gcal-importer.c:40
msgid "Failed to allocate a new object in memory"
msgstr "Bellekte yeni nesne ayrılamadı"

#: src/gui/importer/gcal-importer.c:43
msgid "File is malformed, invalid, or corrupted"
msgstr "Dosya kusurla, geçersiz veya bozuk"

#: src/gui/importer/gcal-importer.c:46
msgid "Failed to parse the calendar contents"
msgstr "Takvim içerikleri ayrıştırılamadı"

#: src/gui/importer/gcal-importer.c:49
msgid "Failed to read file"
msgstr "Dosya okunamadı"

#: src/gui/importer/gcal-importer.c:56
msgid "Internal error"
msgstr "İç hata"

#: src/gui/importer/gcal-importer.c:94
msgid "File is not an iCalendar (.ics) file"
msgstr "Dosya iCalendar (.ics) dosyası değil"

#: src/gui/importer/gcal-import-file-row.c:154
msgid "Description"
msgstr "Açıklama"

#: src/gui/views/gcal-agenda-view.c:366
msgid "On-going"
msgstr "Süren"

#: src/gui/views/gcal-agenda-view.ui:19
msgid "No events"
msgstr "Olay yok"

#: src/gui/views/gcal-month-popover.ui:59
msgid "New Event…"
msgstr "Yeni Olay…"

#: src/gui/views/gcal-week-grid.c:576
msgid "00 AM"
msgstr "ÖÖ 00"

#: src/gui/views/gcal-week-grid.c:579
msgid "00:00"
msgstr "00:00"

#: src/gui/views/gcal-week-header.c:472
#, c-format
msgid "Other event"
msgid_plural "Other %d events"
msgstr[0] "Diğer %d olay"

#: src/gui/views/gcal-week-header.c:1000
#, c-format
msgid "week %d"
msgstr "%d. hafta"

#: src/utils/gcal-utils.c:1317
msgid ""
"The event you are trying to modify is recurring. The changes you have "
"selected should be applied to:"
msgstr ""
"Değiştirmeye çalıştığınız olay yineleniyor. Şeçtiğiniz değişiklikler "
"aşağıdakilere uygulanmalıdır:"

#: src/utils/gcal-utils.c:1322
msgid "_Only This Event"
msgstr "Yalnızca Bu _Olay"

#: src/utils/gcal-utils.c:1329
msgid "_Subsequent events"
msgstr "_Sonraki olaylar"

#: src/utils/gcal-utils.c:1332
msgid "_All events"
msgstr "_Tüm olaylar"
